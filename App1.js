import * as React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { NavigationContainer, DarkTheme } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createStackNavigator } from '@react-navigation/stack';
import { enableScreens } from 'react-native-screens';

import Colors from './components/colors';

enableScreens(false);

//screens
import SettingScreen from './screens/SettingScreen';
import S1 from './screens/s1';
import Register from './screens/Register';
import Login from './screens/login';
import Display from './screens/display';
import Aircraft from './screens/Aircraft';
import SetAircraft from './screens/setAircraft';
import Profile from './screens/profile';
import EGCAUpload from './screens/egcaUpload';
import BuildLogbook from './screens/buildLogbook';

const Tab = createBottomTabNavigator();
function HomeTabs() {
    return (
      <Tab.Navigator initialRouteName="SettingScreen"
      tabBarOptions={{
        activeTintColor: Colors.primary,
      }}>
        {/* <Tab.Screen name="Register" component={Register} options={{
          tabBarLabel: 'Register',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-plus" color={color} size={size} />
          ),
        }} /> */}
        <Tab.Screen name="SettingScreen" component={SettingStackScreen} 
        options={{
            tabBarLabel: 'Settings',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="cog-outline" color={color} size={size} />
            ),
          }} />
          {/* <Tab.Screen name="LogBook" component={LogBookStackScreen} options={{
          tabBarLabel: 'LogBook',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="notebook-multiple" color={color} size={size} />
          ),
        }} /> */}
      </Tab.Navigator>
    );
  }

  const AuthStack = createStackNavigator();
    function AuthStackScreen() {
    return (
    <AuthStack.Navigator initialRouteName="Register">
        <AuthStack.Screen name="Register" component={Register} options={{headerShown: false}} />
        <AuthStack.Screen name="Login" component={Login} options={{headerShown: false}} />
    </AuthStack.Navigator>
    );
    }

    const SettingStack = createStackNavigator();
    function SettingStackScreen({navigation}) {
    return (
    <SettingStack.Navigator navigationOptions ={{
        tintColor: '#e91e63',
    }}>
        <SettingStack.Screen name="Settings" component={S1} options={{headerShown: false}} />
        <SettingStack.Screen name="Display" component={Display} />
        <SettingStack.Screen name="Aircraft"
        component={Aircraft}
        options={{
        headerTitleStyle: { alignSelf: 'center' },
        headerLeft: () => (
            <TouchableOpacity onPress={()=> {navigation.navigate('Display')}}>
            <Text style={{color: 'blue', marginLeft: 10}}>Dismiss</Text>
            </TouchableOpacity>
        ),
        headerRight: () => (
            <TouchableOpacity onPress={()=> {navigation.navigate('SetAircraft')}}>
            <MaterialCommunityIcons name="plus" color='blue' size={30} style={{marginRight: 10}}/>
            </TouchableOpacity>
        ),
        }} />
        <SettingStack.Screen name="SetAircraft"
        component={SetAircraft}
        options={{
        headerTitleStyle: { alignSelf: 'center' },
        headerLeft: () => (
            <TouchableOpacity onPress={()=> {navigation.navigate('Aircraft')}}>
            <Text style={{color: 'blue', marginLeft: 10, fontSize:20}}>Save</Text>
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: '#dcdcdc',
            borderBottomWidth: 1,
            borderBottomColor:'#000'
        },
        headerTintColor: '#dcdcdc',
        }} />
        <SettingStack.Screen name="Profile" component={Profile} />
        <SettingStack.Screen name="EGCAUpload" component={EGCAUpload} />
        <SettingStack.Screen name="BuildLogbook" component={BuildLogbook} />
    </SettingStack.Navigator>
    );
    }
  
  const Stack = createStackNavigator();

  function App() {
    return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Auth">
        <Stack.Screen name="SettingScreen" component={HomeTabs} options={{headerShown:false}}/>
        <Stack.Screen name="Auth" component={AuthStackScreen} options={{headerShown:false}} />
      </Stack.Navigator>
    </NavigationContainer>
    );
  }

  export default App;