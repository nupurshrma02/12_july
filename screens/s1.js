//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';

import Colors from '../components/colors';

// create a component
const S1 = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.settings}>Settings</Text>
            <Text style={styles.mainLine}>Lorem Ipsum</Text>
            <TouchableOpacity style={styles.fields}>
                <View>
                <Text style={styles.text}> Display</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.fields}>
                <Text style={styles.text}> Pilot Details</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.fields}>
                <Text style={styles.text}> Build log Book</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.fields}>
                <Text style={styles.text}> Gallery</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.fields}>
                <Text style={styles.text}> Support</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.fields}>
                <Text style={styles.text}> Backup</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.fields}>
                <Text style={styles.text}> Help Videos</Text>
            </TouchableOpacity>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        padding: 25,
        height: Dimensions.get('window').height,
    },
    settings:{
        fontSize: 28,
        marginTop:10,
        //fontWeight: 'bold',
        marginHorizontal:15,
        color: Colors.primary,
        fontFamily: 'WorkSans-ExtraBold',
    },
    mainLine:{
        color: Colors.accent,
        marginHorizontal:15,
        marginTop:5,
        fontSize: 15,
        fontFamily: 'WorkSans-VariableFont_wght',
    },
    fields: {
        marginTop: 30,
        borderBottomWidth: 1,
        borderBottomColor: Colors.primary,
        width:'100%',
    },
    text:{
        marginBottom: 15,
        fontSize: 15,
        color: Colors.primary,
        //fontWeight: 'bold',
        fontFamily:'WorkSans-Bold'
    },
});

//make this component available to the app
export default S1 
