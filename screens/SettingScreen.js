//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity,Button } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Modal, Portal, Provider } from 'react-native-paper';

const SettingScreen = ({navigation}) => {
  const [visible, setVisible] = React.useState(false);

  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const containerStyle = {backgroundColor: 'white', padding: 20};


     return (
      <Provider>
            <Portal>
            <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
              <Text>Example Modal.  Click outside this area to dismiss.</Text>
            </Modal>
            </Portal>
            <View style={styles.container}>
                <TouchableOpacity onPress={()=> navigation.navigate('Display')}><Text style={styles.text}>Display/Default</Text></TouchableOpacity>
                <TouchableOpacity onPress={()=> navigation.navigate('Profile')}><Text style={styles.text}>Pilot Details/Ecrew-Login/Eupload</Text></TouchableOpacity>
                <TouchableOpacity onPress={()=> navigation.navigate('EGCAUpload')}><Text style={styles.text}>EGCA Upload</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text} onPress={()=> navigation.navigate('BuildLogbook')} >Build LogBook</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text}>Gallery</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text}>Support/Backup</Text></TouchableOpacity>
                <View style={{flexDirection:'row'}}>
                <Text style={styles.text}>Help Videos</Text>
                <TouchableOpacity onPress={showModal}>
                <MaterialCommunityIcons name="head-question" color={'red'} size={30} style={{marginLeft: 200}} />
                </TouchableOpacity>
                </View>           
            </View>
      </Provider>
        );
    }


// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        //backgroundColor: '#2c3e50',
    },
    text: {
        fontSize: 25,
    },

});

//make this component available to the app
export default SettingScreen;
