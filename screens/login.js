//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground , Dimensions } from 'react-native';
import  Colors  from '../components/colors';

// create a component
const Login = () => {
  return (
    <ImageBackground source={require('../images/bg1.png')}
    imageStyle={{
      resizeMode: "cover",
      //alignSelf: "flex-end"
    }}
    style={styles.backgroundImage}>
    <View style={{width:'100%', paddingHorizontal:15,}}>
    <View style={styles.card}>
      <Text style={styles.login}>Login</Text>
      <Text style={styles.mainLine}>Please enter your email & {'\n'} phone</Text>
    </View>
    {/* TextInput */}
    <View>
      
    </View>
    </View>
    </ImageBackground>
  );
};

// define your styles
const styles = StyleSheet.create({
  backgroundImage: {
        flex: 1, 
        alignItems:'center',
        justifyContent: 'center',
        height: Dimensions.get('window').height*1.3,
},

  card:{
    backgroundColor: '#E6FAFF',
    alignItems: 'center',
    borderRadius: 10,
    opacity: 0.9,
    alignItems: 'center',
    width:'100%',
    paddingHorizontal:20,
    paddingVertical: 20,
    shadowOpacity: 0.5,
    shadowColor:'black',
    elevation:8,
  },
  login:{
    fontFamily: 'AbrilFatface-Regular',
    fontSize: 34,
    color: Colors.primary,
  },
  mainLine:{
    textAlign:'center',
    marginTop: 25,
    fontSize: 14,
    color: Colors.accent,
  },

});

//make this component available to the app
export default Login;
