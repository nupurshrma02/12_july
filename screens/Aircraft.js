//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// create a component
const Aircraft = () => {
 const [focused,setFocused] = React.useState(false);

 const onFocusChange = () => setFocused(true);
 const onFocusCancelled = () => setFocused(false);

    return (
        <View style={styles.container}>
            <View style={{backgroundColor:'#c0c0c0', padding:20, flexDirection:'row'}}>
               <View style={(focused) ? styles.searchbar2 : styles.searchbar}>
                 <MaterialCommunityIcons name="magnify" color={'#c0c0c0'} size={25} />
                 <TextInput 
                 onFocus={onFocusChange}
                 placeholder='Aircraft Type' 
                 style={{marginTop: -10, fontSize:15, width:100,}}
                 />
               </View>
               <Text>{focused ? <Text style={styles.cancelButton} onPress={onFocusCancelled}>Cancel</Text> : ''}</Text>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'center',
        //backgroundColor: '#2c3e50',
    },
    searchbar: {
      //paddingLeft: 10,
      backgroundColor: '#fff',
      padding: 10,
      width: '100%',
      borderRadius: 10,
      flexDirection: 'row',
      paddingVertical: 10,
    },
    searchbar2: {
        //paddingLeft: 10,
        backgroundColor: '#fff',
        padding: 10,
        width: '80%',
        borderRadius: 10,
        flexDirection: 'row',
        paddingVertical: 10,
      },
      cancelButton: {
          fontSize: 20,
          marginLeft: 10,
          marginTop: 10,
      },
});

//make this component available to the app
export default Aircraft;
