//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { RadioButton } from 'react-native-paper';

// create a component
const EGCAUpload = () => {
    const [flight, setFlight] = React.useState('third');

    return (
        <View style={styles.container}>
            <View style={styles.flightContainer}>
                <Text style={{fontSize:15,}}>FlightType/PurposeDetails</Text>
                <Text>Flight Details</Text>
                <View style= {styles.roleView}>
                  <RadioButton
                        value="first"
                        status={ flight === 'first' ? 'checked' : 'unchecked' }
                        onPress={() => setFlight('first')}
                    />
                    <Text style={styles.fieldText}>Training</Text>
                  <RadioButton
                        value="second"
                        status={ flight === 'second' ? 'checked' : 'unchecked' }
                        onPress={() => setFlight('second')}
                    />
                    <Text style={styles.fieldText}>Test</Text>
                    </View>
                    <View style= {styles.roleView}>
                    <RadioButton
                            value="third"
                            status={ flight === 'third' ? 'checked' : 'unchecked' }
                            onPress={() => setFlight('third')}
                        />
                        <Text style={styles.fieldText}>Commercial</Text>
                    <RadioButton
                            value="fourth"
                            status={ flight === 'fourth' ? 'checked' : 'unchecked' }
                            onPress={() => setFlight('fourth')}
                        />   
                        <Text style={styles.fieldText}>Non-commercial</Text>
                  </View>                  
                </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        //backgroundColor: '#2c3e50',
    },
    flightContainer:{
        padding: 80,
        backgroundColor: '#fff',
        width:'90%',
        marginTop:10,
        borderRadius:10,
        
    },
    roleView: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        padding: 10,
        //width: '100%',
        //borderBottomColor:'grey',
        //borderBottomWidth: 1,
    },
    fieldText: {
        fontSize: 18,
        marginTop: 5,
        },
});

//make this component available to the app
export default EGCAUpload;
