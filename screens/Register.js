//import liraries
import React, { Component } from 'react';
import { View, 
Text,
StyleSheet, 
TextInput, 
KeyboardAvoidingView, 
Platform,
TouchableWithoutFeedback,
Keyboard ,
TouchableOpacity,
ScrollView,
Dimensions
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Picker} from '@react-native-picker/picker';
import { ScaledSheet } from 'react-native-size-matters';

import Colors from '../components/colors';

// create a component
const Register = ({navigation}) => {
  const [selectedCity, setSelectedCity] = React.useState('');
    return (
        
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ScrollView>
        <KeyboardAvoidingView
        keyboardVerticalOffset={-500} 
        behavior= "height"
        style={styles.container}>
          <View>
           <MaterialCommunityIcons 
           name="arrow-left" color='#000' size={30} style={styles.arrow}
           />
           <Text style={styles.welcome}>Welcome</Text>
           <Text style={styles.mainLine}>Login with your account to</Text>
           {/* TextInput */}
           <View style = {styles.inputBox}>
             <Icon name="user" size={20} color='#266173' style={styles.icon} />
             <TextInput style={styles.textInputBox} placeholder='Name' placeholderTextColor = "#266173"/>
           </View>

           <View style = {styles.inputBox}>
             <Icon name="envelope" size={20} color='#266173' style={styles.icon} />
             <TextInput style={styles.textInputBox} placeholder='Email' placeholderTextColor = "#266173"/>
           </View>

           <View style = {styles.inputBox}>
            <MaterialCommunityIcons 
             name="lock-outline" color='#266173' size={20} style={styles.icon}/>
             <TextInput style={styles.textInputBox} placeholder='Password' placeholderTextColor = "#266173"/>
           </View>

           <View style = {styles.inputBox}>
            <MaterialCommunityIcons 
             name="lock-outline" color='#266173' size={20} style={styles.icon}/>
             <TextInput style={styles.textInputBox} placeholder='Confirm Password' placeholderTextColor = "#266173"/>
           </View>
           
           {/* country, CountryCode */}
           <View style={{flexDirection:'row', justifyContent:'space-between'}}>
              <View style={styles.CountryBox}>
              <MaterialCommunityIcons 
                name="map-marker-radius-outline" color='#266173' size={20} style={styles.icon}/>
              <Picker
              selectedValue={selectedCity}
              onValueChange={(itemValue, itemIndex) =>
                  setSelectedCity(itemValue)
              }
              style={{marginLeft: '90%',}}>
              <Picker.Item label="India" value="India" />
              <Picker.Item label="Afghanistan" value="Afghanistan" />
              <Picker.Item label="Aland Islands" value="Aland Islands" />
              </Picker>
              </View>
              <View style={styles.CountryCodeBox}>
              <MaterialCommunityIcons 
                name="xml" color='#266173' size={20} style={styles.icon}/>
              <TextInput 
              style={{}}
              placeholder='Country code' 
              placeholderTextColor = "#266173"
              />
              </View>
           </View>

           <View style = {styles.inputBox}>
            <MaterialCommunityIcons 
             name="cellphone" color='#266173' size={20} style={styles.icon}/>
             <TextInput style={styles.textInputBox} 
             placeholder='Mobile No.' 
             placeholderTextColor = "#266173"
             />
           </View>
        </View>
          
           <TouchableOpacity onPress={()=>navigation.navigate('SettingScreen')}>
               <View style={styles.button}>
                   <Text style={styles.buttonText}>Register</Text>
               </View>
           </TouchableOpacity>

           <View style={{paddingTop:20,flexDirection:'row'}} >
              <Text style={{textAlign:'center'}}>Don't have an account?</Text>
              <TouchableOpacity onPress={()=>navigation.navigate('Login')}>
                <Text style={styles.login}>Login</Text>
              </TouchableOpacity>
           </View>
           
        </KeyboardAvoidingView>
        </ScrollView>
        </TouchableWithoutFeedback>
        
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: 15,
        height: Dimensions.get('window').height,
    },
    arrow:{
      //marginTop: 50,
      //marginLeft:20,
    },
    welcome:{
        fontSize: 28,
        marginTop:10,
        //fontWeight: 'bold',
        marginHorizontal:5,
        color: Colors.primary,
        fontFamily: 'WorkSans-ExtraBold',
        
    },
    mainLine:{
        color: Colors.accent,
        marginHorizontal:5,
        marginTop:10,
        fontSize: 15,
        fontFamily: 'WorkSans-VariableFont_wght',
    },
    inputBox: {
        flexDirection: 'row',
        marginTop:20,
        borderBottomWidth: 0.8,
        //minWidth: 300,
        width:'100%',
        //width: Dimensions.get('window').width,
        maxWidth:'100%'
    },
    CountryBox:{
        flexDirection: 'row',
        marginTop:20,
        borderBottomWidth: 0.8,
        // minWidth: 100,
        width:'40%',
        // maxWidth:'90%'
    },
    CountryCodeBox:{
        flexDirection: 'row',
        marginTop:20,
        borderBottomWidth: 0.8,
        paddingLeft:10,
        // minWidth: 100,
        width:'50%',
        // maxWidth:'90%'
    },
    icon: {
      marginHorizontal:5,
      marginTop: 15,
    },
    button:{
        backgroundColor: Colors.primary,
        marginTop: 50,
        padding: 15,
        alignItems:'center',
        borderRadius:10,
        //marginVertical: 20,
        width: '100%',
        //marginLeft:20,
        minWidth: 330,
        maxWidth: '100%',
    },
    buttonText:{
        color: '#fff'
    },
    login:{
        fontSize: 15,
        color: 'blue',
        textAlign:'center',
        marginLeft: 10,
    },
    textInputBox:{
        width: '100%'
    },
});

//make this component available to the app
export default Register;
