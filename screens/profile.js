//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, ScrollView, TouchableOpacity, } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
import { RadioButton } from 'react-native-paper';

// create a component
const Profile = () => {
    const [selectedCity, setSelectedCity] = React.useState('');
    const [airComp, setAirComp] = React.useState();
    const [flight, setFlight] = React.useState('third');

    const [image, setImage] = React.useState(null);
    const [imageData, setImageData] = React.useState('');
    const [imageFilename, setImageFilename] = React.useState('');

    const [date, setDate] = React.useState('');


    const selectingImage = () => {
        ImagePicker.showImagePicker({quality: 0.3}, responseGet => {
          console.log('Response = ', responseGet);
   
          if (responseGet.didCancel) {
            console.log('User cancelled image picker');
          } else if (responseGet.error) {
            console.log('ImagePicker Error: ', responseGet.error);
          } else {
            const source = {uri: responseGet.uri};
   
            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            // this.setState({
            //   setImage: source,
            //   imageData: responseGet.data,
            //   imageFilename: responseGet.fileName,
            // });
            console.log(source)
               setImage(source)

          }
        });
      };

    return (
    <ScrollView>
        <View style={styles.container}>
            {/* Profile update */}
                <TouchableOpacity onPress={()=>selectingImage()}>
                    <Image source={
                         image === null
                           ? require('../images/user.png')
                           : image
                       } 
                    style={{height:70, width:70}}/>
                </TouchableOpacity> 
            <View style={styles.inputBox}>
            <TextInput placeholder='Name'/>
            </View>
            <View style={styles.inputBox}>
            <TextInput placeholder='Email'/>
            </View>
            <View style={styles.inputBox}>
            <TextInput placeholder='Licence Type'/>
            </View>
            <View style={styles.inputBox}>
            <TextInput placeholder='Licence Number'/>
            </View>
            <View style={styles.inputBox}>
            <DatePicker
          style={styles.datePickerStyle}
          date={date} // Initial date from state
          mode="date" // The enum of date, datetime and time
          placeholder="validity"
          format="DD-MM-YYYY"
          //minDate="01-01-2016"
          //maxDate="01-01-2019"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              //display: 'none',
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
          onDateChange={(date) => {
            setDate(date);
          }}
        />
            </View>
            <View style={styles.inputBox}>
            <Picker
            selectedValue={selectedCity}
            onValueChange={(itemValue, itemIndex) =>
                setSelectedCity(itemValue)
            }>
            <Picker.Item label="India" value="India" />
            <Picker.Item label="Afghanistan" value="Afghanistan" />
            <Picker.Item label="Aland Islands" value="Aland Islands" />
            </Picker>
            </View>
            <View style={{flexDirection:'row',}}>
               <View style={styles.countryCode}>
                   <TextInput placeholder='+91'/>
               </View>
               <View style={styles.Phone}>
                   <TextInput placeholder='Mobile'/>
               </View>
            </View>
            <TouchableOpacity>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Save</Text>
                </View>
            </TouchableOpacity>

            {/* Change Password */}
            <Text style={{marginTop:20, fontWeight:'bold', fontSize: 30}}>Change-Password</Text>
            <View style={styles.inputBox}>
            <TextInput placeholder='Old Password'/>
            </View>
            <View style={styles.inputBox}>
            <TextInput placeholder='New Password'/>
            </View>
            <View style={styles.inputBox}>
            <TextInput placeholder='Confirm Password'/>
            </View>
            <TouchableOpacity>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Change</Text>
                </View>
            </TouchableOpacity>

            {/* Ecrew Login */}
            <Text style={{marginTop:20, fontWeight:'bold', fontSize: 30}}>Ecrew-Login</Text>
            <View style={styles.inputBox}>
            <TextInput placeholder='Crew Id'/>
            </View>
            <View style={styles.inputBox}>
            <TextInput placeholder='Crew Password'/>
            </View>
            <View style={styles.inputBox}>
            <Picker
            selectedValue={airComp}
            onValueChange={(itemAir, itemIndex) =>
                setAirComp(itemAir)
            }>
            <Picker.Item label="Air Berlin " value="Air Berlin" />
            <Picker.Item label="Indigo" value="Indigo" />
            <Picker.Item label="Spicejet" value="Spicejet" />
            </Picker>
            </View>
            <TouchableOpacity>
                <View style={styles.button}>
                <Text style={styles.buttonText} >Roster Import</Text>
                </View>
            </TouchableOpacity>

            {/* Egca upload */}
            <Text style={{marginTop:20, fontWeight:'bold', fontSize: 30}}>EGCA Upload</Text>
            
            <View style={styles.flightContainer}>
                <Text style={{fontSize:15,}}>FlightType/PurposeDetails</Text>
                <Text>Flight Details</Text>
                <View style= {styles.roleView}>
                  <RadioButton
                        value="first"
                        status={ flight === 'first' ? 'checked' : 'unchecked' }
                        onPress={() => setFlight('first')}
                    />
                    <Text style={styles.fieldText}>Training</Text>
                  <RadioButton
                        value="second"
                        status={ flight === 'second' ? 'checked' : 'unchecked' }
                        onPress={() => setFlight('second')}
                    />
                    <Text style={styles.fieldText}>Test</Text>
                    </View>
                    <View style= {styles.roleView}>
                    <RadioButton
                            value="third"
                            status={ flight === 'third' ? 'checked' : 'unchecked' }
                            onPress={() => setFlight('third')}
                        />
                        <Text style={styles.fieldText}>Commercial</Text>
                    <RadioButton
                            value="fourth"
                            status={ flight === 'fourth' ? 'checked' : 'unchecked' }
                            onPress={() => setFlight('fourth')}
                        />   
                        <Text style={styles.fieldText}>Non-commercial</Text>
                  </View>                  
                </View>

        </View>
    </ScrollView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        //backgroundColor: '#2c3e50',
    },
    inputBox: {
       borderWidth: 1,
       borderRadius: 10,
       width: '80%',
       marginTop: 10,
    },
    countryCode: {
        borderWidth: 1,
        borderRadius: 10,
        width: '30%',
        marginTop: 10,
    },
    Phone: {
        borderWidth: 1,
        borderRadius: 10,
        width: '50%',
        marginTop: 10,
        
    },
    button: {
        backgroundColor:'red',
        padding: 10,
        marginTop: 10,
        width: 100,
        borderRadius:10,
        alignItems:'center'
    },
    buttonText:{
      fontWeight: 'bold',
      color: '#fff',
    },
    datePickerStyle: {
        width: '100%',
        borderRadius: 10,
        //marginTop: 20,
      },
      flightContainer:{
        padding: 80,
        backgroundColor: '#fff',
        width:'90%',
        marginTop:10,
        borderRadius:10,
        marginBottom:10,
        
    },
    roleView: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        padding: 10,
        //width: '100%',
        //borderBottomColor:'grey',
        //borderBottomWidth: 1,
    },
    fieldText: {
        fontSize: 18,
        marginTop: 5,
        },
});

//make this component available to the app
export default Profile;
