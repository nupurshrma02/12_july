//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { RadioButton } from 'react-native-paper';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';

// create a component
const SetAircraft = ({navigation}) => {
    const [category, setCategory] = React.useState('first');
    const [engine, setEngine] = React.useState('engineFirst');
    const [Class, setClass] = React.useState('ClassFirst');
    const [image, setImage] = React.useState(null);
    const [imageData, setImageData] = React.useState('');
    const [imageFilename, setImageFilename] = React.useState('');
    


    const selectingImage = () => {
        ImagePicker.showImagePicker({quality: 0.3}, responseGet => {
          console.log('Response = ', responseGet);
   
          if (responseGet.didCancel) {
            console.log('User cancelled image picker');
          } else if (responseGet.error) {
            console.log('ImagePicker Error: ', responseGet.error);
          } else {
            const source = {uri: responseGet.uri};
   
            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            // this.setState({
            //   setImage: source,
            //   imageData: responseGet.data,
            //   imageFilename: responseGet.fileName,
            // });
            console.log(source)
               setImage(source)

          }
        });
      };

        return (
            <View style={styles.container}>
                <View style={{padding: 50, backgroundColor: '#fff', width:'100%', alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>selectingImage()}>
                    <Image source={
                         image === null
                           ? require('../images/2.png')
                           : image
                       } 
                         style={{height:70, width:70}}/>
                    </TouchableOpacity>     
                </View>
            {/* Aircraft */}

            <Text style={styles.text}>Aircraft</Text>
             <ScrollView>
                <View style={styles.fields}>
                    <Text> Aircraft Type</Text>
                    <TextInput placeholder='Aircraft Type' style={{marginLeft: 180, marginTop: -10}}/>
                </View>
                <View style={styles.fields}>
                    <Text> Aircraft ID</Text>
                    <TextInput placeholder='Aircraft ID' style={{marginLeft: 200, marginTop: -10}}/>
                </View>
            {/* Category */}

            <Text style={styles.text}>Category</Text>
            <View style= {styles.roleView}>
                  <RadioButton
                        value="first"
                        status={ category === 'first' ? 'checked' : 'unchecked' }
                        onPress={() => setCategory('first')}
                    />
                    <Text style={styles.fieldText}>Air Plane</Text>
                  <RadioButton
                        value="second"
                        status={ category === 'second' ? 'checked' : 'unchecked' }
                        onPress={() => setCategory('second')}
                    />
                    <Text style={styles.fieldText}>Microlight</Text>
                  <RadioButton
                        value="third"
                        status={ category === 'third' ? 'checked' : 'unchecked' }
                        onPress={() => setCategory('third')}
                    />
                    <Text style={styles.fieldText}>Helicopter</Text>
                  <RadioButton
                        value="fourth"
                        status={ category === 'fourth' ? 'checked' : 'unchecked' }
                        onPress={() => setCategory('fourth')}
                        style={{marginTop: 30,}}
                    />   
                    <Text style={styles.fieldText}>Glider</Text>
                  </View>
                {/* Engine */}

                <Text style={styles.text}>Engine</Text>
                <View style= {styles.roleView}>
                <RadioButton
                        value="engineFirst"
                        status={ engine === 'engineFirst' ? 'checked' : 'unchecked' }
                        onPress={() => setEngine('engineFirst')}
                    />
                    <Text style={styles.fieldText}>Jet</Text>
                  <RadioButton
                        value="engineSecond"
                        status={ engine === 'engineSecond' ? 'checked' : 'unchecked' }
                        onPress={() => setEngine('engineSecond')}
                    />
                    <Text style={styles.fieldText}>Turbo Prop</Text>
                  <RadioButton
                        value="engineThird"
                        status={ engine === 'engineThird' ? 'checked' : 'unchecked' }
                        onPress={() => setEngine('engineThird')}
                    />
                    <Text style={styles.fieldText}>Turbo-shaft</Text>
                </View>
                <View style= {styles.roleView}>
                <RadioButton
                        value="engineFourth"
                        status={ engine === 'engineFourth' ? 'checked' : 'unchecked' }
                        onPress={() => setEngine('engineFourth')}
                    />
                    <Text style={styles.fieldText}>Piston</Text>
                  <RadioButton
                        value="engineFifth"
                        status={ engine === 'engineFifth' ? 'checked' : 'unchecked' }
                        onPress={() => setEngine('engineFifth')}
                    />
                    <Text style={styles.fieldText}>Not Powered</Text>
                </View>
                <View style= {styles.roleView}>
                    <Text>Engine Name</Text>
                    <TextInput placeholder='Engine Name' style={{marginLeft: 180, marginTop: -10}} />
                </View>
                {/* Class */}

                <View style= {styles.roleView}>
                    <Text style={styles.fieldText}>Class</Text>
                    <RadioButton
                            value="ClassFirst"
                            status={ Class === 'ClassFirst' ? 'checked' : 'unchecked' }
                            onPress={() => setClass('ClassFirst')}
                        />
                        <Text style={styles.fieldText}>ME Land</Text>
                    <RadioButton
                            value="ClassSecond"
                            status={ Class === 'ClassSecond' ? 'checked' : 'unchecked' }
                            onPress={() => setClass('ClassSecond')}
                        />
                        <Text style={styles.fieldText}>ME Sea</Text> 
                </View>
                <View style= {styles.roleView}>
                    <RadioButton
                            value="ClassThird"
                            status={ Class === 'ClassThird' ? 'checked' : 'unchecked' }
                            onPress={() => setClass('ClassThird')}
                        />
                        <Text style={styles.fieldText}>SE Land</Text>
                    <RadioButton
                            value="ClassFourth"
                            status={ Class === 'ClassFourth' ? 'checked' : 'unchecked' }
                            onPress={() => setClass('ClassFourth')}
                        />
                        <Text style={styles.fieldText}>SE Sea</Text> 
                </View>
                </ScrollView>
            </View>
        );
    }


// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'center',
        //backgroundColor: '#2c3e50',
    },
    text: {
      fontSize: 15,
      color: 'grey',
      fontWeight: 'bold',
      marginTop: 10,
      marginLeft: 10,
      marginBottom: 5,
    },
    fields:{
      padding: 5,
      backgroundColor: '#fff',
      borderBottomColor: '#000',
      borderBottomWidth: 1,
      flexDirection:'row',
    },
    roleView: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        padding: 10,
        width: '100%',
        borderBottomColor:'grey',
        borderBottomWidth: 1,
    },
    fieldText: {
        fontSize: 15,
        marginTop: 5,
        },
});

//make this component available to the app
export default SetAircraft;
